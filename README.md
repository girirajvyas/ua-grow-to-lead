# Grow to Lead #
This repository is to consolidate all the materials and learnings that are acquired during the complete program.

### What is this repository for? ###

 - Module 1 - Management Foundation             | Practice 1 - Manager Identity
 - Module 1 - Management Foundation             | Practice 2 – Managing Your Time and Stress
 - Module 2 - Team Dynamics                     | Practice 3 – Helping Your Team Develop
 - Module 3 - Team Management Basics            | Practice 4 – Managing Team Performance Remotely
 - Module 3 - Team Management Basics            | Practice 5 – Leading Productive Online Meetings
 - Module 4 - Motivating and engaging your team | Practice 6 – Value of Motivation
 - Module 5 - Conflict Management               | Practice 7 – Effective Conflict Resolution

### Expert sessions
 1. Effective Feedback

### Discussion club
 1. Building and Earning Trust

### Foundation courses ( Mandatory before the program start)

There are 3 obligatory LinkedIn courses that are the theoretical basis for the practical sessions  

 - Management Foundations - https://www.linkedin.com/learning/management-foundations-5/the-task-and-people-balance?u=2113185
 - Building Your Team - https://www.linkedin.com/learning/building-your-team/forming-stage?u=2113185
 - Critical Thinking - https://www.linkedin.com/learning/critical-thinking/welcome-to-critical-thinking?u=2113185

## Building your team:

**Use a personality assessment tool for your self awareness:**
 - DiSC
 - Myers-Briggs Type indicator
 - Stengthfinder

**SWOT Analysis for Deeper understanding of strengths**  
 - Steangths
 - Weaknesses
 - Opportunities
 - Threats

**Stages of team development**  
 - Forming
    - Clarifyy team purpose
    - Set ground rules
 - Storming
 - Norming
 - Performing

**Project management software**  
 - basecamp
 - asana

## Critical Thinking

**Tools for critical thinking**  
 - 5 whys
 - 7 so whats
 - Blowing up the business
 - problem soping

**Pitfalls:**  
 - Jumping to answers too quickly
 - Unwilling to expand the problem space
 - Focussing on unimportant

**Summary from critical thinking e-learning**  

|Sr. no| Term used | Definition                                                                                                                                                       |
|-----:| ----------|:-------------:                                                                                                                                                    |
|  1   | 5 whys    | An analytical technique where you ask “Why?” five times to understand why something is happening. This approach can lead you to the issue’s root cause|
|  2   | 7 so whats| An analytical technique where you ask “So what?” seven times to understand the future implications of any actions you might take. This helps you identify possible new problems or opportunities that will arise from your actions|
|  3   | 80/20 rule| Also known as the Pareto principle, this rule suggests that 80% of the effects of an issue come from 20% of the issue’s causes.|
|  4   | root cause| The earliest underlying factor contributing to a problem|


# Module 1 - Management Foundation | Practice 1 - Manager Identity (24/09/2020)

## Pre-assignment: study
 - https://www.linkedin.com/learning/new-manager-foundations-2/establishing-your-identity-as-a-manager?u=2113185
 - Article Module1_Practise1_Manager_Identity_24_Sept_preassignment_article.pdf in sessions folder


## From Session
Manager responsibilities:  
 - Prioritizing
 - dont try to solve everything at once

Lead  
 - Assign easiest task to yourself
 - Take responsibility of team

## Post assignment: practice
**Option 1:**  
for participants who have project pool  
list 3-5 main challenges/ changes you experienced when you became a TL. What advice/recommendation would you give for novice leaders?  

**Option 2:**  
for Participants who don't have project pool  
Let's Imagine you receive a new team on a new project. List what action you would take in the first month.

# Module 1 - Management Foundation | Practice 2 – Managing Your Time and Stress (08/10/2020)

## Pre-assignment: study
**Mandatory:**  
 - Short video: https://www.linkedin.com/learning/linkedin-learning-highlights-business-strategy-and-analysis/managing-stress-and-avoiding-burnout?u=2113185
 - Short article: https://clockify.me/time-management-techniques
 - Module1_Practice2_Manager_Identity_08_Oct_preassignment_article.pdf

**Optional:**  
 - https://www.thebalancecareers.com/time-management-techniques-2276138
 - https://www.youtube.com/watch?v=RcGyVTAoXEU - How to make stress your friend, Kelly McGonigal

**Key takeaways from above self study**  
 - Control the alarm brain (Stress causes alarm brain takeover) - Stress reduction helps smart brain functioning.
 - Identify your triggers - Identify your most common triggers at work and in life.
 - Pause before reacting - Take breaks for stronger focus
 - Change habits by changing thoughts - Shift from "I always fail at the end moment" to "I will succeed" or "I can do it"

## From Session


## Post assignment: practice

**For those who do not have a team:**  
Review your tasks/to-dos/responsibilities based on the 4D system and act accordingly. Share your experiences in a few sentences in Teams.  

 - `Delete it:` What are the consequences of not doing the task at all? Consider the 80/20 rule; maybe it doesn’t need to be done in the first place.
 - `Delegate it:` If the task is important, ask yourself if it’s really something that you are responsible for doing in the first place. Can the task be given to someone else?
 - `Do it now:` Postponing an important task that needs to be done only creates feelings of anxiety and stress. Do it as early in the day as you can.
 - `Defer:` If the task is one that can’t be completed quickly and is not a high priority item, simply defer it.

**For those who have a team:**
Review a task/goal with one of your team members. Help him create a priority list and a timeline.  
OR  
Delegate a complex task to one of your team members. Make sure you delegate in a proper way: you explain why, how to do it, what is in it for the person, create timelines and evaluate the progress/succes

# Module 2 - Team Dynamics  | Practice 3 – Helping Your Team Develop (15/10/2020)  

## Pre-assignment: study
**Mandatory:**  

**Optional:**  



## Post assignment: practice

Based on the pre-assignment and your takeaways from the practice session, describe for yourself the answers to the following questions:

 - Which stage is your own team at?
 - How can you experience that?
 - How can you help your team move to the next stage?
 - What is your first step?
 - What changes do you expect in team dynamics?

# Module 3 - Team Management Basics | Practice 4 – Managing Team Performance Remotely (22/10/2020)

## Pre-assignment: study

## From Session

 - KPIs (Key performance indicators) or KRA (Key result areas)
 
 - 
 - 
 
Advantages of leading team remotely
 - Self management/VUCA training


Question: What metrics/KPIs would you use for a scrum team of 5 people?
 - 
 - 
## Post assignment: practice


# Module 3 - Team Management Basics  | Practice 5 – Leading Productive Online Meetings (01/10/2020)  

## Pre-assignment: study
**Mandatory:**  
 - https://www.linkedin.com/learning/management-foundations-5/your-role-in-meetings?u=2113185
 - Module3_Practise5_Manager_Identity_01_Oct_preassignment_ article_Having productive meetings.pdf

**Optional:**  
 - Common Meeting Problems | Chris Croft - https://www.linkedin.com/learning/common-meeting-problems/welcome?u=2113185
 - Leading Productive Meetings| Dave Crenshaw - https://www.linkedin.com/learning/leading-productive-meetings-3/improve-meetings?u=2113185

**Your role in meetings:**  
 - Have a clear desired outcome
 - Have a fewer meetings
 - Be a facilitator
 - Talk less and ask more

## From Session
Learning Objectives
 - learn conduct effective meeting
 
Agenda
 - Meeting simulations
 - 11 ways 
 - Challenging behaviours

Strategies for shorter meetings
11 strategies for shorter meetings - https://tomlaforce.com/11-strategies-for-shorter-meetings/

## Post assignment: practice

**For those who have team:**  
 - Review with the team online meeting rules or Conduct with the team virtual team building

**For those who don't have team:**  
 - Create the list of personal rules for online meetings or Conduct with the team virtual team building

# Module 4 - Motivating and engaging your team | Practice 6 – Value of Motivation (28/10/2020)

## Pre-assignment: study
**Mandatory**
https://www.linkedin.com/learning/motivating-and-engaging-employees-2/welcome?u=2113185

Engaging employees:
3 common aspects :
 - Strong communicators
 - they build on their people's streangth
 - manage performance

 


**Optional**



## From Session
What is Motivation?


Challenges of motivation:

Internal Motivation
 - 

External motivation
 - punishment or reward


Motivating the team

How can we deal with motivation in a distrbuted team
 - Voluntary tasks a person takes and the quality he delivers
 - 

Motivating the individual
 - by Power and status
 - by achievements
 - by relations/affiliations 

Motivation modedels:
 - Maslow pyramid
 - herxberg model



-----------------Task
How do you understand all are motivated?
 - by quality of work

What will you do to identify motivation gaps?
 - 1 2 1 discussions, identify goals for them to motivate
 - Sending anonymous surveys
 - Check if self initiatives are taken

How will you monitor motivation level and compensate if any issues?
 - Help them reach their goals
 - Participation of team members in meetings
 - Team working independently

## Post Assignment
**For those who have team:**  

Analyze the existing approaches you use to motivate and engage your team members;
Create action plan, that helps you to improve motivation process while working remotely;
Implement

**For those who don't have team:**  
Analyze the existing approaches that are used in your current project/team and describe them
Create at least 2 points for improvement and describe how? and why?


# Good Reads:
 - https://tomlaforce.com/11-strategies-for-shorter-meetings/
 - https://liberationist.org/7-ways-to-promote-psychological-safety-in-the-workplace/
 - https://workingmouse.com.au/innovation/agile-lean-scrum-kaizen-new-names-same-faces
 - https://www.kitchensoap.com/2012/10/25/on-being-a-senior-engineer/
 
# Books: 
 - 7 habits of highly effective people
 - Extreme ownership - how us nave seals win
 - The Power of No: Because One Little Word Can Bring Health, Abundance, and Happiness - James Altucher